const express = require('express')
const orders = require('./routes/orders')

const app = express()
const port = 3000

// Landing page
app.get('/', (req, res) => {
  res.send('Hello Aston!')
})

// Router
app.use('/orders', orders)

// Wildcard for unknown URLs
app.get('*', (req, res) => {
  res.status(404).send('<iframe src="https://giphy.com/embed/aYpmlCXgX9dc09dbpl" class="center" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p></p>')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
