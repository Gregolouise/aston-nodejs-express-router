# README #

Correction de l'exercice visant à mettre en place un premier serveur NodeJs / Express

### What was asked ###

1. Lancer un serveur express (https://expressjs.com/fr/starter/installing.html) et le pousser dans un repo Git public

2. Installation de Nodemon (https://www.npmjs.com/package/nodemon)

3. Une page d’acceuil (‘/’) affichant votre nom / prenom

4. Mise en place d’un routeur, avec log de la date (Date.now() ) et de l’url appelée (middlewares)

5. Création d’un CRUD sur la route /orders dans un fichier orders.js, appelé dans le index.js :
 
	GET /orders avec le message “Voici la liste des orders” Prevoir l’utilisation de QueryStringParameters

	GET /orders/id dynamique avec le message “Voici l’order d’id :“ affichant la valeur de l’id
 	
	POST /orders avec le message “Order d’id X créée”, avec X la valeur de l’id en generant une valeur d’uuid 
		(https://www.npmjs.com/package/uuid)
 	
	UPDATE /orders/id avec le message “Order d’id X mis à jour” 

	DELETE /orders/id avec le message “Order d’id X supprimée”

6. Un middleware renvoyant un code d’erreur 404 un un message “Page not found” pour toutes les autres routes


### Who do I talk to? ###

* gregoire.louise@lapilulerouge.io