const express = require('express')
const router = express.Router()
const { v4: uuidv4 } = require('uuid')

router.use(function (req, res, next) {
  console.log('Timestamp is : ', Date.now())
  next()
})

router.use(function (req, res, next) {
  console.log('Method : ' + req.method + ' on Endpoint : ' + req.baseUrl + req.url)
  next()
})

// Get All orders
router.get('/', function (req, res) {
  // You can find queryStringParameters in req.query
  console.log(req.query)
  res.status(200).send('orders list')
})

// Get Order by Id
router.get('/:id', function (req, res) {
  // You can find pathParameters in req.params
  console.log(req.params)
  const id = req.params.id
  res.status(200).send('Order id : ' + id)
})

// Post a new Order
router.post('/', function (req, res) {
  const id = uuidv4()
  res.status(201).send('Order with id : ' + id + ' created')
})

// Put Order by Id
router.put('/:id', function (req, res) {
  const id = req.params.id
  res.status(200).send('Order with id : ' + id + ' updated')
})

// Delete Order bu Id
router.delete('/:id', function (req, res) {
  res.status(204).send()
})

module.exports = router
